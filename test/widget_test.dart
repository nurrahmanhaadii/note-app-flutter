// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:first_flutter/presentation/home/home_page.dart';
import 'package:first_flutter/presentation/home/home_page_test.dart';
import 'package:first_flutter/presentation/menu1/menu1_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:first_flutter/main.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mockingjay/mockingjay.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });

  testWidgets('description', (WidgetTester tester) async {
    // final navigator = MockNavigator();
    // when(() => navigator.push(any())).thenAnswer((_) async {});

    await tester.pumpWidget(const ProviderScope(child: MyApp()));
    expect(find.byType(BottomNavigationBar), findsOneWidget);
    expect(find.text('Home'), findsOneWidget);
    await tester.tap(find.text('List'));
    await tester.tap(find.text('Home'));

    await tester.pumpWidget(const MaterialApp(
      home: HomePage()
      ),
    );
    expect(find.text('Menu 1'), findsOneWidget);
    await tester.tap(find.byType(TextButton));
    await tester.pumpWidget(const MaterialApp(
        home: Menu1Page()
    ),
    );
    //
    // verifyNever(
    //       () => navigator.push(any(that: isRoute<void>(whereName: equals('/menu1_page')))),
    // ).called(0);

    // expect(find.text('Menu 1'), findsOneWidget);
    // expect(find.byType(FloatingActionButton), findsOneWidget);
  });

  testWidgets('pushes SettingsPage when TextButton is tapped', (tester) async {
    final navigator = MockNavigator();
    when(() => navigator.push(any())).thenAnswer((_) async {
      return null;
    });

    await tester.pumpWidget(
      MaterialApp(
        home: MockNavigatorProvider(
          navigator: navigator,
          child: const MyHomePage(),
        ),
      ),
    );

    await tester.tap(find.byType(TextButton));

    verifyNever(
          () => navigator.push(any(that: isRoute<void>(whereName: equals('/settings')))),
    );
  });
}
