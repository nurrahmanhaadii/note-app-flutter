import 'package:first_flutter/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AddPage extends ConsumerStatefulWidget {
  const AddPage({Key? key}) : super(key: key);

  @override
  _AddPageState createState() => _AddPageState();
}

class _AddPageState extends ConsumerState<AddPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      body: body(context),
    );
  }

  AppBar appBar() {
    return AppBar(
      title: const Text("Add Note"),
      centerTitle: true,
    );
  }

  Widget body(context) {
    String titleText = "";
    String descriptionText = "";

    return Padding(
      padding: const EdgeInsets.only(
        top: 15,
        right: 15,
        left: 15,
        bottom: 80,
      ),
      child: Column(
        children: [
          TextField(
            decoration: const InputDecoration(
                border: InputBorder.none,
                hintText: "Title",
                hintStyle: TextStyle(fontSize: 20, color: Colors.black)),
            style: const TextStyle(fontSize: 20, color: Colors.black),
            onChanged: (value) {
              titleText = value;
            },
          ),
          Expanded(
              child: TextField(
                decoration: const InputDecoration.collapsed(
                    hintText: 'Enter Description',
                    border: InputBorder.none,
                    hintStyle: TextStyle(fontSize: 18, color: Colors.black)),
                style: const TextStyle(fontSize: 18, color: Colors.black),
                onChanged: (value) {
                  descriptionText = value;
                },
              )),
          TextButton(onPressed: () {
            ref.watch(noteProvider.notifier).addNote(titleText, descriptionText);
            Navigator.pop(context);
          }, child: const Text("Add Note", style: TextStyle(color: Colors.white),),
            style: TextButton.styleFrom(backgroundColor: Colors.blueAccent, padding: const EdgeInsets.all(12)),)
        ],
      ),
    );
  }

}
