import 'package:first_flutter/domain/models/note.dart';
import 'package:first_flutter/presentation/add/add_page.dart';
import 'package:first_flutter/presentation/general/notes_card.dart';
import 'package:first_flutter/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class Menu1Page extends ConsumerStatefulWidget {
  const Menu1Page({Key? key}) : super(key: key);

  @override
  _Menu1PageState createState() => _Menu1PageState();
}

class _Menu1PageState extends ConsumerState<Menu1Page> {

  @override
  Widget build(BuildContext context) {
    ref.watch(deleteProvider.notifier).setStatus(true);
    return Scaffold(
      appBar: appBar(),
      body: body(),
      floatingActionButton: fab(context),
    );
  }

  AppBar appBar() {
    return AppBar(
      title: const Text(
        "Menu 1",
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
      ),
      centerTitle: true,
    );
  }

  Widget body() {
    final notes = ref.watch(noteProvider);
    if (notes.isNotEmpty) {
      return ListView.builder(
          itemCount: notes.length,
          itemBuilder: (context, index) {
            return NotesCard(notes[index]);
          });
    } else {
      return const Center(
        child: Text("No Data"),
      );
    }
  }

  Widget fab(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => const AddPage()));
      },
      child: const Icon(Icons.add, color: Colors.blueGrey, size: 30,),
      backgroundColor: Colors.white,
    );
  }
}
