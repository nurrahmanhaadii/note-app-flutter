import 'package:first_flutter/presentation/general/notes_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../provider.dart';

class ListPage extends ConsumerStatefulWidget {
  const ListPage({Key? key}) : super(key: key);

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends ConsumerState<ListPage> {

  @override
  Widget build(BuildContext context) {
    ref.watch(deleteProvider.notifier).setStatus(false);
    return Scaffold(
      body: SafeArea(child: body(),)
    );
  }

  AppBar appBar() {
    return AppBar(
      title: const Text(
        "Menu 1",
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
      ),
      centerTitle: true,
    );
  }

  Widget body() {
    final notes = ref.watch(noteProvider);
    if (notes.isNotEmpty) {
      return ListView.builder(
          itemCount: notes.length,
          itemBuilder: (context, index) {
            return NotesCard(notes[index]);
          });
    } else {
      return const Center(
        child: Text("No Data"),
      );
    }
  }
}
