import 'package:first_flutter/presentation/menu1/menu1_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class HomePage extends ConsumerStatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ConsumerState<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            buildMenu("Menu 1", () {
              Navigator.push(context, MaterialPageRoute(
                  builder: (context) => Menu1Page())
              );
            }),
            const SizedBox(width: 12.0,),
            buildMenu("Menu 2", () {
              
            })
          ],
        ),
      ),
    );
  }

  Widget buildMenu(String title, Function callback) {
    return GestureDetector(
        onTap: () {
          callback();
        },
        child: CircleAvatar(
          radius: 32.0,
          child: Text(title),
        ));
  }
}


class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TextButton(
        onPressed: () => Navigator.of(context).push(MySettingsPage.route()),
        child: const Text('Navigate'),
      ),
    );
  }
}

class MySettingsPage extends StatelessWidget {
  const MySettingsPage({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute(
      builder: (_) => const MySettingsPage(),
      settings: const RouteSettings(name: '/settings'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold();
  }
}
