import 'package:first_flutter/presentation/menu1/menu1_page.dart';
import 'package:flutter/material.dart';

class HomePageTest extends StatefulWidget {
  const HomePageTest({Key? key}) : super(key: key);

  @override
  _HomePageTestState createState() => _HomePageTestState();
}

class _HomePageTestState extends State<HomePageTest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            buildMenu("Menu 1", () {
              Navigator.push(context, MaterialPageRoute(
                  builder: (context) => Menu1Page())
              );
            }),
            const SizedBox(width: 12.0,),
            buildMenu("Menu 2", () {

            })
          ],
        ),
      ),
    );
  }

  Widget buildMenu(String title, Function callback) {
    return GestureDetector(
        onTap: () {
          callback();
        },
        child: CircleAvatar(
          radius: 32.0,
          child: Text(title),
        )
    );
  }
}
