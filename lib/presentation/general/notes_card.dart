import 'package:first_flutter/domain/models/note.dart';
import 'package:first_flutter/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class NotesCard extends ConsumerWidget {
  final Note note;

  const NotesCard(this.note, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      margin: const EdgeInsets.all(15),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.grey,
        borderRadius: BorderRadius.circular(15),
      ),
      child: row(ref),
    );
  }

  Widget row(WidgetRef ref) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                note.title,
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 5),
              Text(
                note.description,
                style: const TextStyle(fontSize: 17),
              ),
            ],
          ),
        ),
        Visibility(
          visible: ref.watch(deleteProvider),
          child: Center(
              child: IconButton(
            onPressed: () {
              ref.watch(noteProvider.notifier).deleteNote(note);
              final value = ref.watch(noteProvider);
            },
            icon: const Icon(Icons.delete),
          )),
        ),
      ],
    );
  }
}
