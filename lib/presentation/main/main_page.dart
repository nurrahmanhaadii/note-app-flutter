import 'package:first_flutter/infrastructure/network_status_service.dart';
import 'package:first_flutter/presentation/home/home_page.dart';
import 'package:first_flutter/presentation/list/list_page.dart';
import 'package:first_flutter/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MainPage extends ConsumerStatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends ConsumerState<MainPage> {
  List<Widget> listWidget = [
    const HomePage(),
    const ListPage(),
  ];

  List<BottomNavigationBarItem> bottomNavBarItems = [
    const BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: "Home",
    ),
    const BottomNavigationBarItem(
      icon: Icon(Icons.assignment),
      label: "List",
    )
  ];

  void _onBottomNavTapped(int index) {
    ref.watch(bottomProvider.notifier).updateState(index);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body(),
      bottomNavigationBar: BottomNavigationBar(
        items: bottomNavBarItems,
        onTap: _onBottomNavTapped,
        currentIndex: ref.watch(bottomProvider),
      ),
    );
  }

  Widget body() {
    final network = ref.watch(netWorkProvider);
    return network.when(loading: () => const CircularProgressIndicator(),
      error: (err, stack) => Text('Error: $err'),
    data: (status) {
      return dataWidget(status);
    });
  }

  Widget dataWidget(NetworkStatus networkStatus) {
    if (networkStatus == NetworkStatus.Online) {
      _showToastMessage("Online");
      return listWidget[ref.watch(bottomProvider)];
    } else {
      _showToastMessage("Offline");
      return listWidget[ref.watch(bottomProvider)];
    }
  }

  void _showToastMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1);
  }
}