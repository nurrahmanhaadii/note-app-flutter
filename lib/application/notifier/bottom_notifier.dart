import 'package:flutter_riverpod/flutter_riverpod.dart';

class BottomNotifier extends StateNotifier<int> {
  BottomNotifier() : super(0);

  void updateState(int index) => state = index;
}