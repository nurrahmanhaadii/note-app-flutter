
import 'package:first_flutter/domain/models/note.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class NoteNotifier extends StateNotifier<List<Note>> {
  NoteNotifier() : super(<Note>[]);

  void addNote(String title, String description) {
    state = [...state, Note(title, description)];
  }
  
  void deleteNote(Note note) {
    state = state.where((Note currentNote) => note != currentNote).toList();
  }

}