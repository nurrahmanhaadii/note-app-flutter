import 'package:flutter_riverpod/flutter_riverpod.dart';

class DeleteNotifier extends StateNotifier<bool> {
  DeleteNotifier() : super(false);

  void setStatus(bool isMenu1Page) {
    state = isMenu1Page;
  }
}