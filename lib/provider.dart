
import 'package:first_flutter/application/notifier/bottom_notifier.dart';
import 'package:first_flutter/application/notifier/delete_notifier.dart';
import 'package:first_flutter/application/notifier/note_notifier.dart';
import 'package:first_flutter/infrastructure/network_status_service.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'domain/models/note.dart';

final noteProvider = StateNotifierProvider<NoteNotifier, List<Note>>((ref) => NoteNotifier());

final bottomProvider = StateNotifierProvider.autoDispose<BottomNotifier, int>((ref) => BottomNotifier());

final deleteProvider = StateNotifierProvider.autoDispose<DeleteNotifier, bool>((ref) => DeleteNotifier());

final netWorkProvider = StreamProvider((ref) => NetworkStatusService().networkStatusController.stream);


